var user_id;

function bindEmptyContainer(container_no1,container_no2,that){
  getApp().sp();
  user_id = wx.getStorageSync('user_id');
  wx.request({
    url: getApp().globalData.apiurl + 'bindEmptyContainer',
    method: 'POST',
    data: {
      "user_id": user_id,
      "container_no1": container_no1,
      "container_no2": container_no2                                                                                                             
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {

      getApp().hp();
      if (res.data.isSuccess == 1) {
        getApp().alert("空箱绑定成功！")
        setTimeout(function(){
          wx.navigateBack({
            delta: 1
          })
        },1500);
      }else{
        getApp().alert(res.data.res);
      }
    }
  })
}


function containerTest(container_no){
  wx.request({
    url: getApp().globalData.apiurl + 'containerTest',
    method: 'POST',
    data: {
      "container_no": container_no                                                                                                         
    },
    header: {
      "content-type": "application/x-www-form-urlencoded",
    },
    success: function (res) {

      return 1;
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    addresses: ["请选择"],
    addressIndex: 0,
    inputVal1:"",
    inputVal2:""
  },
  inputTyping1: function (e) {
    this.setData({
      inputVal1: e.detail.value.toUpperCase()
    });
  },
  inputTyping2: function (e) {
    this.setData({
      inputVal2: e.detail.value.toUpperCase()
    });
  },
  addressChange: function(e) {
    this.setData({
        addressIndex: e.detail.value
    })
  },
  onLoad: function (options) {
  
  },
  onShow: function () {
    var that = this;
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  bind: function (e) {
    var that = this;
    var post = e.detail.value;
    var container_no1 = "";
    var container_no2 = "";
    if( ( post.container_no1!="" && !/^\d{7}$/.test(post.container_no1) && !/^[A-Z]{4}\d{7}$/.test(post.container_no1) ) || ( post.container_no2!="" && !/^\d{7}$/.test(post.container_no2) && !/^[A-Z]{4}\d{7}$/.test(post.container_no2) ) ){  
      getApp().alert('请输入7位数字或4位字母+7位数字');return;
    }
    if (post.container_no1 == post.container_no2 ) {
      getApp().alert("请不要输入两个相同的箱号！");return;
    }
    if (post.container_no1 != "") {
      var res;
      wx.request({
        url: getApp().globalData.apiurl + 'containerTest',
        method: 'POST',
        data: {
          "container_no":post.container_no1                                                                                                         
        },
        header: {
          "content-type": "application/x-www-form-urlencoded",
        },
        success: function (ress) {
          res = ress.data;
          console.log(res);
          if (res.isSuccess == 0) {
            getApp().alert(res.res);
            return;
          }else if (res.isSuccess == 2) {
            var content = "";
            for (var i = 0; i < res.container_no.length; i++) {
              content = content+""+res.container_no[i]+"，";
            }

            getApp().alert("您输入的第一个箱号"+post.container_no1+"有多种可能的字母，分别为："+content+"请输入完整的11位箱号进行录入！");
            return;
          }else if (res.isSuccess == 1){
            container_no1 = res.container_no[0];
            //console.log(res);
            that.setData({
              inputVal1:container_no1
            })
            if (post.container_no2 != "") {
              var res;
              wx.request({
                url: getApp().globalData.apiurl + 'containerTest',
                method: 'POST',
                data: {
                  "container_no":post.container_no2                                                                                                         
                },
                header: {
                  "content-type": "application/x-www-form-urlencoded",
                },
                success: function (ress) {
                  res = ress.data;
                  if (res.isSuccess == 0) {
                    getApp().alert(res.res);return;
                  }else if (res.isSuccess == 2) {
                    var content = "";
                    for (var i = 0; i < res.container_no.length; i++) {
                      content = content+""+res.container_no[i]+"，";
                    }
                    getApp().alert("您输入的第二个箱号"+post.container_no1+"有多种可能的字母，分别为："+content+"请输入完整的11位箱号进行录入！");return
                  }else if (res.isSuccess == 1){
                    container_no2 = res.container_no[0];
                    that.setData({
                      inputVal2:container_no2
                    })
                    bindEmptyContainer(container_no1,container_no2,that);
                    
                  }
                }
              })
            }else{
              bindEmptyContainer(container_no1,"",that);
            }

          }
        }
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})