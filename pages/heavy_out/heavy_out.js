var voucherImg = []
var id1 = ''
var id2 = ''
var container_list = [] // 箱号列表
var order = {}
var isAddMedia = false
var containerCheck = require('../../utils/containerCheck.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAddMedia: false,
    isEdit1: false,
    isEdit2: false,
    list1: [],
    list2: [],
    inputVal1: "",
    inputVal2: "",
    voucher_img: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    isAddMedia = !!options.isAddMedia // 是否编辑模式
    order = {}
    voucherImg = []
    container_list = []
    console.log(isAddMedia)
    if (isAddMedia) {
      order = JSON.parse(options.order)
      this.initByOrder(order)
    } else {
      this.getAllSubOrderToDriverAndSubOrderList()
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  initByOrder: function (thatOrder) {
    let that = this
    wx.request({
      url: getApp().globalData.javaurl + '/main/driverOrderDetail/getDriverOrderDetailListByPage',
      method: 'POST',
      data: {
        "id": thatOrder.id,
        "pageSize": 0
      },
      header: {
        "content-type": "application/x-www-form-urlencoded",
        "Authorization": wx.getStorageSync('token')
      },
      success: function (res) {
        if (res.data.data.total === 1) {
          let order = res.data.data.rows[0]
          console.log(order)
          that.setData({
            isAddMedia: true,
            id: order.id
          })
          if (order.containerNo) {
            that.setData({
              inputVal1: order.containerNo
            })
          }
          if (order.voucherImg) {
            voucherImg = JSON.parse(order.voucherImg)
            that.setData({
              voucher_img: voucherImg
            })
          }
        }
      }
    })
  },

  getAllSubOrderToDriverAndSubOrderList: function () {
    let that = this
    wx.request({
      url: getApp().globalData.javaurl + '/main/driverOrderDetail/getHeavyOutByDriverOrderLIstByPage',
      method: 'POST',
      data: {
        // "stateStr": "1",
        "pageSize": 0,
        stateStr:1
      },
      header: {
        "content-type": "application/x-www-form-urlencoded",
        "Authorization": wx.getStorageSync('token')
      },
      success: function (res) {
        container_list = []
        if(!( res.data.data.rows.length>0)){
          getApp().alert("您目前没有单！")
        }
        res.data.data.rows.forEach((item) => {
          container_list.push({'container_no': item.containerNo, 'id': item.id})
        })
        that.setData({
          list1: container_list,
          list2: container_list
        })
      }
    })
  },
  getDriverOrderById: function (id) {
    console.log('id', id)
    let theId = id
  },
  inputTyping1: function (e) {
    let list_now = []
    let inputVal = e.detail.value.toUpperCase()
    container_list.forEach(function (data) {
      if (data.container_no && data.container_no.indexOf(inputVal) >= 0) {
        list_now.push(data)
      }
    })
    this.setData({
      inputVal1: inputVal,
      list1: list_now,
      isEdit1: true
    })
    if (e.detail.value === "") {
      id1 = ''
      this.setData({
        list1: container_list,
        isEdit1: false
      })
    }
  },
  setVal1: function (e) {
    id1 = e.currentTarget.dataset.id
    this.setData({
      inputVal1: e.currentTarget.dataset.container_no,
      isEdit1: false
    })
  },
  inputTyping2: function (e) {
    let list_now = []
    let inputVal = e.detail.value.toUpperCase()
    container_list.forEach(function (data) {
      if (data.container_no && data.container_no.indexOf(inputVal) >= 0) {
        list_now.push(data)
      }
    })
    this.setData({
      inputVal2: inputVal,
      list2: list_now,
      isEdit2: true
    })
    if (e.detail.value === "") {
      id2 = ''
      this.setData({
        list2: container_list,
        isEdit2: false
      })
    }
  },
  setVal2: function (e) {
    id2 = e.currentTarget.dataset.id
    this.setData({
      inputVal2: e.currentTarget.dataset.container_no,
      isEdit2: false
    })
  },
  /**
   * TODO(选择媒体文件上传)
   * @auther zhangQ
   * @date 2018/3/24 18:12
   */
  chooseMedia: function () {
    let that = this
    wx.showActionSheet({
      itemList: ['拍摄照片上传', '拍摄视频上传'],
      success: function (res) {
        if (res.tapIndex === 0) {
          that.choosePhoto()
        } else if (res.tapIndex === 1) {
          that.chooseVideo()
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },
  /**
   * TODO(选择图片上传)
   * @auther zhangQ
   * @date 2018/3/24 18:12
   */
  choosePhoto: function () {
    let that = this
    wx.chooseImage({
      count: 6,
      sizeType: ['compressed'],
      success: function (res) {
        that.uploadFiles(res.tempFilePaths, 0)
      }
    })
  },
  /**
   * TODO(选择视频上传)
   * @auther zhangQ
   * @date 2018/3/24 18:12
   */
  chooseVideo: function () {
    let that = this
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 10,
      camera: 'back',
      success: function (res) {
        wx.uploadFile({
          url: getApp().globalData.apiurl + 'uploadFileReturnUrl',
          filePath: res.tempFilePath,
          name: 'file',
          success: function (res) {
            let resData = JSON.parse(res.data)
            let obj = {
              type: 'video',
              src: resData.src
            }
            voucherImg.push(obj)
          },
          complete: function (res) {
            that.setData({
              voucher_img: voucherImg
            })
          }
        })
      }
    })
  },
  /**
   * TODO(多张图片上传)
   * @auther zhangQ
   * @date 2018/4/23 11:20
   */
  uploadFiles: function (imgs, index) {
    let that = this
    wx.uploadFile({
      url: getApp().globalData.apiurl + 'uploadFileReturnUrl',
      filePath: imgs[index],
      name: 'file',
      success: function (res) {
        console.log(1)
        console.log(res.data)
        console.log(voucherImg)
        let resData = JSON.parse(res.data)
        let obj = {
          type: 'img',
          src: resData.src
        }
        voucherImg.push(obj)
        console.log(voucherImg)
      },
      complete: function (res) {
        console.log(2)
        console.log(voucherImg)
        index++
        console.log(index)
        that.setData({
          voucher_img: voucherImg
        })
        let thatthis = that
        if (index < imgs.length) {
          setTimeout(function () {
            thatthis.uploadFiles(imgs, index)
          }, 200)
        }
      }
    })
  },
  bind: function (e) {
    let post = e.detail.value
    if (isAddMedia) {
      this.updateMediaInfo(post.id, voucherImg)
    } else {
      if (!containerCheck.methods.checkDigit(post.container_no1)) {
        getApp().alert("箱号1输入错误！")
        return
      } else {
        let is_in = false
        container_list.forEach(function (data) {
          if (data.container_no === post.container_no1) {
            is_in = true
          }
        })
        if (!is_in) {
          getApp().alert("箱号1输入错误！请选择列表中的箱号！")
          return
        }
      }
      if (!post.container_no2) {
        post.container_no2 = ""
      } else {
        if (!containerCheck.methods.checkDigit(post.container_no2)) {
          getApp().alert("箱号2输入错误！")
          return
        } else {
          let is_in = false
          container_list.forEach(function (data) {
            if (data.container_no === post.container_no2) {
              is_in = true
            }
          })
          if (!is_in) {
            getApp().alert("箱号2输入错误！请选择列表中的箱号！")
            return
          }
        }
      }
      if (post.container_no1 === post.container_no2) {
        getApp().alert("请不要输入两个相同的箱号！")
        return
      }

      this.saveSubOrder(post.container_no1, post.container_no2, voucherImg)
    }
  },
  saveSubOrder: function (container_no1, container_no2, mVoucherImg) {
    getApp().sp("加载中...")
    wx.request({
      url: getApp().globalData.javaurl + '/main/driverOrderDetail/saveDriverOrder',
      method: 'POST',
      data: {
        containerNo1: container_no1,
        containerNo2: container_no2,
        voucherImg: JSON.stringify(mVoucherImg)
      },
      header: {
        "content-type": "application/x-www-form-urlencoded",
        "Authorization": wx.getStorageSync('token')
      },
      success: function (res) {
        if (+res.data.isSuccess === 1) {
          wx.showModal({
            title: '提示',
            content: '上传成功',
            success: function (res) {
              wx.navigateBack({
                delta: 1
              })
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '上传失败',
            success: function (res) {
            }
          })
        }
      },
      complete: function (res) {
        getApp().hp()
      }
    })
  },
  updateMediaInfo: function (id, mVoucherImg) {
    getApp().sp("加载中...")
    wx.request({
      url: getApp().globalData.javaurl + '/main/driverOrderDetail/addMediaInfo',
      method: 'POST',
      data: {
        id: id,
        voucherImg: JSON.stringify(mVoucherImg)
      },
      header: {
        "content-type": "application/x-www-form-urlencoded",
        "Authorization": wx.getStorageSync('token')
      },
      success: function (res) {
        if (+res.data.isSuccess === 1) {
          wx.showModal({
            title: '提示',
            content: '上传成功',
            success: function (res) {
              wx.navigateBack({
                delta: 1
              })
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '上传失败',
            success: function (res) {
            }
          })
        }
      },
      complete: function (res) {
        getApp().hp()
      }
    })
  }
})