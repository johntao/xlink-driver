var user_id = wx.getStorageSync('user_id');
var like = "";

function getChatList (that) {
    getApp().sp("加载中");
    wx.request({
        url: getApp().globalData.javaurl + '/common/chat/getChatList',
        method: 'POST',
        header: {
            "content-type": "application/x-www-form-urlencoded",
            "Authorization": wx.getStorageSync('token')
        },
        success: function (res) {
            getApp().hp();
            wx.stopPullDownRefresh();
            let resGroup = []
            res.data.data.group.forEach(function (item) {
                if (item.type === 1) {
                    item.showContent = item.content
                } else if (item.type === 2) {
                    item.showContent = '声音'
                } else if (item.type === 3) {
                    item.showContent = '图片'
                } else {
                    item.showContent = item.content
                }
                resGroup.push(item)
            })
            let resFriend = []
            
            res.data.data.friend.forEach(function (item) {
                if (item.type === 1) {
                    item.showContent = item.content
                } else if (item.type === 2) {
                    item.showContent = '声音'
                } else if (item.type === 3) {
                    item.showContent = '图片'
                } else {
                    item.showContent = item.content
                }
                resFriend.push(item)
            })
            //按时间排序
            for (let s_i = 0; s_i < resFriend.length-1;s_i++)
            {
              for (let s_j = 0; s_j < resFriend.length - 1 - s_i;s_j++)
              {
                 //console.log(resFriend[s_i].time);
                if (resFriend[s_j].time < resFriend[s_j+1].time)
                {
                  //console.log(resFriend[s_j].time, resFriend[s_j + 1].time);
                  let temp = resFriend[s_j];
                  resFriend[s_j] = resFriend[s_j + 1];
                  resFriend[s_j + 1] = temp;
                }
              }
            }

            that.setData({
                friend: resFriend,
                group: resGroup
            });
            console.log(res.data);

        }
    })
}

Page({

    /**
     * 页面的初始数据
     */
    data: {},

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this;
        getChatList(that);
        getApp().addListenerchatlist(function (data) {
            if (data.friend.length > 0) {
                console.log(data.friend);
            }
            if (data.group.length > 0) {
                console.log(data.group);
            }

        });
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        getChatList(that);
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})