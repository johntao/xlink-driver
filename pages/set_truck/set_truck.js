var all = [];
var search = [];
var values = [];
var user_id;

function setTruck(id,that){
  user_id = wx.getStorageSync('user_id');
  getApp().sp("加载中...");
  wx.request({
    url: getApp().globalData.apiurl + 'setTruck',
    method: 'POST',
    data: {
      "user_id": user_id,
      "id":id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      getApp().hp();
      if (res.data.isSuccess == 1) {
        getApp().alert('设置成功！');
        setTimeout(function(){
          wx.navigateBack({
            delta: 1
          })
        },1500);

      }else{
        getApp().alert(res.data.res);
      }
    }
  })
}


function getTruckList(that){
  user_id = wx.getStorageSync('user_id');
  getApp().sp("加载中...");
  wx.request({
    url: getApp().globalData.apiurl + 'getTruckList',
    method: 'POST',
    data: {
      "user_id": user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      getApp().hp();
      if (res.data.isSuccess == 1) {
        var login_name = wx.getStorageSync('login_name');
        for (var i = 0; i < res.data.data.length ; i++) {

          res.data.data[i].checked = false;
          if (res.data.data[i].driver_name == login_name) {
            res.data.data[i].checked = true;
          }
        }
        all = res.data.data;
        search = res.data.data;
        that.setData({
          trucks : search
        });
        console.log(res.data);
      }else{
        getApp().alert(res.data.res);
      }
      
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputShowed: false,
    inputVal: ""
  },
  showInput: function () {
      this.setData({
          inputShowed: true
      });
  },
  hideInput: function () {
      this.setData({
          inputVal: "",
          inputShowed: false
      });
  },
  clearInput: function () {
      this.setData({
          inputVal: ""
      });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });

    search = [];
    for (var i = 0; i < all.length; i++) {

      if(all[i].truck_no.indexOf(e.detail.value) >= 0){
        search.push(all[i]);
      }
    }
    this.setData({
      trucks : search
    });
      
  },
  radioChange: function (e) {
      console.log(e.detail.value);
      for (var i = 0 ; i < all.length; i++) {
        if (all[i].id == e.detail.value) {
          all[i].checked = true;
        }else{
          all[i].checked = false;
        }
      }

      this.setData({
          trucks: search
      });
  },

  checkboxChange: function (e) {
    console.log( e.detail.value);

    values = e.detail.value;

    for (var i = 0 ; i < all.length ; i++) {
        all[i].checked = false;

        for (var j = 0 ; j < values.length ; j++) {
            if(all[i].id == values[j]){
                all[i].checked = true;
                break;
            }
        }
    }

    this.setData({
      trucks : search
    });
  },
  onShow: function () {
    var that = this;
    getTruckList(that);
  },
  set_truck:function(e){
    var id = e.detail.value.id;
    console.log(id);
    var that = this;
    setTruck(id,that);
  }
})