var order_no;
var voucher_img = []

function getSubOrderInfo (that) {
    getApp().sp("加载中...");
    wx.request({
        url: getApp().globalData.apiurl + 'getSubOrderInfo',
        method: 'POST',
        data: {
            "order_no": order_no
        },
        header: {
            "content-type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
            that.setData({
                data: res.data.data
            })
            getApp().hp();
        }
    })
}

/**
 * TODO(上传媒体文件)
 * @auther zhangQ
 * @date 2018/3/23 13:14
 */
function saveSubOrder (that) {
    getApp().sp("加载中...");
    var sendData = {
        order_no: order_no,
        voucher_img: []
    }
    var data = that.data.data
    data.voucher_img.forEach(function (data) {
        sendData.voucher_img.push({ type: data.type, src: data.src })
    })
    sendData.voucher_img = JSON.stringify(sendData.voucher_img)
    wx.request({
        url: getApp().globalData.apiurl + 'saveSubOrder',
        method: 'POST',
        data: sendData,
        header: {
            "content-type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
            wx.showModal({
                title: '提示',
                content: '上传成功',
                success: function (res) {
                    wx.navigateBack({
                        delta: 1
                    })
                }
            })
        },
        complete: function (res) {
            getApp().hp();
        }
    })
}

Page({

    /**
     * 页面的初始数据
     */
    data: {
        data: {
            voucher_img: []
        }
    },
    onLoad: function (options) {
        order_no = options.order_no;
        voucher_img = []
        var that = this
        getSubOrderInfo(that)
    },
    onShow: function () {
    },
    choose: function () {
        var that = this;
        wx.showActionSheet({
            itemList: ['拍摄照片上传', '拍摄视频上传'],
            success: function (res) {
                if (res.tapIndex == 0) {
                    that.photo();
                } else if (res.tapIndex == 1) {
                    that.video();
                }
            },
            fail: function (res) {
                console.log(res.errMsg)
            }
        })
    },
    photo: function () {
        var that = this;
        console.log(that);
        wx.chooseImage({
            count: 1,
            sizeType: ['compressed'],
            sourceType: ['camera'],
            success: function (res) {
                wx.uploadFile({
                    url: getApp().globalData.apiurl + 'uploadFileReturnUrl',
                    filePath: res.tempFilePaths[0],
                    name: 'file',
                    success: function (res) {
                        var resData = JSON.parse(res.data);
                        var obj = {};
                        obj.type = "img"
                        obj.src = resData.src
                        voucher_img.push(obj)
                    },
                    complete: function (res) {
                        var data = that.data.data;
                        data.voucher_img = voucher_img
                        that.setData({
                            data: data
                        })
                    }
                })
            }
        })
    },
    video: function () {
        var that = this;
        wx.chooseVideo({
            sourceType: ['album', 'camera'],
            maxDuration: 10,
            camera: 'back',
            success: function (res) {
                wx.uploadFile({
                    url: 'https://tp.x-link.cc/DriverApi/' + 'uploadFileReturnUrl',
                    filePath: res.tempFilePath,
                    name: 'file',
                    success: function (res) {
                        var resData = JSON.parse(res.data);
                        var obj = {};
                        obj.type = "video";
                        obj.src = resData.src;
                        voucher_img.push(obj)
                    },
                    complete: function (res) {
                        var data = that.data.data;
                        data.voucher_img = voucher_img
                        that.setData({
                            data: data
                        })
                    }
                })

            }
        })
    },
    submitUpload: function () {
        saveSubOrder(this)
    }
})