var pickupService = require('../../service/pickupService.js');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: ["待接", "已接","全部"],
    activeIndex: 0,
    sliderLeft: 0,
    sliderOffset: (wx.getSystemInfoSync().windowWidth / 3 - 84) / 2,
    state : 0,
    orders :[],
    pageNumber:1,
    inputVal:"",
    inputShowed:false,
    more:0,
    stateNames:{
      0:"待接",
      1:"已经接",
      2:"已撤销"
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      pageNumber:1,
      orders: []
    })
    this.loadData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },




  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      pageNumber: 1,
      orders: []
    })
    this.loadData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    if(this.data.more==0)
    {
      this.setData({
        pageNumber: that.data.pageNumber+1,
      })
      this.loadData();
    }
  },
  //切换状态
  tabClick: function (e) {
    if (e.currentTarget.id == 0) {
      this.data.state = 0;
    } else if (e.currentTarget.id == 1) {
      this.data.state = 1
    }else{
      this.data.state = null;
    }
    
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft + (wx.getSystemInfoSync().windowWidth / 3 - 84)/ 2,
      activeIndex: e.currentTarget.id
    });
    this.onPullDownRefresh();
  },
  loadData(){
    let that = this;
    let params ={
      pageNumber: that.data.pageNumber,
      like: that.data.inputVal
    };
    if (that.data.state!=null)
    {
      params.state = that.data.state;
    }

    pickupService.getOrderList(params).then((res)=>{
      wx.stopPullDownRefresh();
      console.log(res);
      that.setData({
        orders: that.data.orders.concat(res.data.data.rows)
      })
      console.log(that.data.orders);
      if (res.data.data.total==0)
      {
        that.setData({
          more: 0
        })
      }else{
        that.setData({
          more: -1
        })
      }

    },(err)=>{
      console.log(err);
    })
  },
  searchInput: function () {
    this.setData({
      inputShowed: true
    })
  },
  hideInput: function () {
    this.setData({
      inputShowed: false
    })
    this.clearInput()
    // 回显old driverList
    //this.backDisplayDriverList()

  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    })
    this.backDisplayDriverList()
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    })
    if (!(e.detail.value + '')) {
      this.backDisplayDriverList()
      return
    }
    this.setData({
      pageNumber: 1,
      orders: []
    })
    this.loadData();
  },
  backDisplayDriverList:function(){
    console.log("backDisplayDriverList");
    this.setData({
      pageNumber: 1,
      orders: []
    })
    this.loadData();
  }, 
  tapAcceptOrder:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    var index = e.currentTarget.dataset.index;
    wx.showModal({
      title: '信息',
      content: "确认是否接单",
      cancelText: "取消",
      confirmText: "确认",
      //confirmColor: "#c71d23",
      success: function (res) {
        if (res.confirm) {
          //提交信息
          let params = {
            id: id
          }
          pickupService.setAcceptSubOrder(params).then((res)=>{
            //拉去新数据
            that.setData({
              pageNumber: 1,
              orders: []
            })
            that.loadData();
            getApp().alert("接单成功！");
          },(err)=>{
            getApp().alert("系统错误！");
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  tapCancleOrder:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    var index = e.currentTarget.dataset.index;
    wx.showModal({
      title: '警告！',
      content: "确认是否撤销该订单！",
      cancelText: "我再想想",
      confirmText: "确定撤销",
      confirmColor: "#c71d23",
      success: function (res) {
        if (res.confirm) {
          //提交信息
          let params = {
            id: id
          }
          pickupService.setCancleSubOrder(params).then((res) => {
            //拉去新数据
            that.setData({
              pageNumber: 1,
              orders: []
            })
            that.loadData();
            getApp().alert("撤销成功！");
          }, (err) => {
            getApp().alert("系统错误！");
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  }
})