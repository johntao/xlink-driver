var user_id = wx.getStorageSync('user_id');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    types:["请选择","公司","个体"],
    typeIndex:0,
    img1_upload:0,
    img2_upload:0,
    img1:"http://47.93.204.205/Public/xcximg/head.png",
    img2:"http://47.93.204.205/Public/xcximg/head.png"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return getApp().share();
  },
  typeChange: function(e){
    this.setData({
        typeIndex: e.detail.value
    })
  },
  chooseimage:function(e){
    var that = this;
    user_id = wx.getStorageSync('user_id');
    wx.chooseImage({
      success: function(res) {
        var tempFilePaths = res.tempFilePaths
        wx.uploadFile({
          url: getApp().globalData.apiurl+'company_img_add',
          filePath: tempFilePaths[0],
          name: 'img',
          formData:{
            'user_id': user_id,
            "num":e.currentTarget.dataset.id
          },
          success: function(res){
            console.log(res);
            if (res.data == 1) {
              if (e.currentTarget.dataset.id == 1) {
                that.setData({
                  img1_upload:1,
                  img1:tempFilePaths[0]
                })

              }else{
                that.setData({
                  img2_upload:1,
                  img2:tempFilePaths[0]
                }) 
               
              }
            }else{
              getApp().alert(res.data);
            }
          }
        })
      }
    })
  },
  formSubmit:function(e){
    getApp().set_form_id(1,e.detail.formId);
    var value = e.detail.value;
    console.log(value);
    if (value.type == 0) {
      getApp().alert('请选择公司类型！');
      return;
    }
    if (value.company_name == "") {
      getApp().alert('请输入公司名称！');
      return;
    }
    if (value.address == "") {
      getApp().alert('请输入公司注册地！');
      return;
    }
    if (value.boss_name == "") {
      getApp().alert('请输入公司法人名称！');
      return;
    }
    if (value.content == "") {
      getApp().alert('请输入公司营业范围！');
      return;
    }
    if (this.data.img1_upload == 0) {
      getApp().alert('请上传营业执照！');
      return;
    }
    if (this.data.img2_upload == 0) {
      getApp().alert('请上传法人身份证号！');
      return;
    }
    wx.showLoading({
      title: '加载中',
    })
    user_id = wx.getStorageSync('user_id');
    wx.showNavigationBarLoading();
    wx.request({
      url: getApp().globalData.apiurl + 'bind_company',
      method: 'POST',
      data: {
        type:value.type,
        company_name:value.company_name,
        type2:value.type2,
        address:value.address,
        boss_name:value.boss_name,
        content:value.content,
        user_id:user_id
      },
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        wx.hideLoading();
        wx.stopPullDownRefresh();
        wx.hideNavigationBarLoading();
        console.log(res.data);
        if (res.data.isSuccess == 1) {
          wx.showToast({
            title: '发布成功',
            icon: 'success',
            duration: 800
          })
          wx.navigateBack({
            delta: 2
          })
        }else{
          getApp().alert(res.data.msg);
        }
        
      }
    })

  }
})