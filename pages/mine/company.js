var user_id = wx.getStorageSync('user_id');

function GetList(that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'company',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      console.log(res.data);
      var company = res.data.company;
      var user = res.data.user;
      that.setData({
        company: company,
        user: user
      });
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
    }
  })
}


// pages/mine/company.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    company:null,
    user:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    GetList(that);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return getApp().share();
  },
  look_pic:function(e){
    wx.previewImage({
      current: e.currentTarget.dataset.src, // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.src] // 需要预览的图片http链接列表
    })
  },
  bindcompany: function () {
    if (this.data.company == null) {
      wx.navigateTo({
        url: 'bind_company'
      })
    }
    
  }
})