var user_id = wx.getStorageSync('user_id');
var content = [];
var app = getApp();

function GetList(that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'user_bbc',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
      if (res.data.isSuccess == 1) {
        for (var i = 0; i < res.data.data.length; i++) {
          content.push(res.data.data[i]);
        }
        that.setData({
          content: content
        });
      }else{
        getApp().alert(res.data.msg);
      }
      
      
    }
  })
}

function del(id,that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.showLoading({
    title: '加载中',
    mask:true
  })
  wx.request({
    url: getApp().globalData.apiurl + 'bbc_del',
    method: 'POST',
    data: {
      "user_id":user_id,
      "id":id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      content = [];
      GetList(that);
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
      wx.hideLoading();
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },
  add:function(){
    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      app.alert('请先登录');
      app.wxLogin();
    } else {
      wx.navigateTo({
        url: '/pages/bbc/add'
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    content = [];
    var that = this;
    GetList(that);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    content = [];
    var that = this;
    GetList(that);

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    GetList(that);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return getApp().share();
  },
  del: function (e) {
    var that = this;
    wx.showModal({
      title: '提示',
      content: '删除后无法恢复，确认删除？',
      success: function(res) {
        if (res.confirm) {
          del(e.target.dataset.id,that);
        }
      }
    })
  },
  look: function (e) {
    wx.navigateTo({
      url: '/pages/bbc/look?id='+e.target.dataset.id
    })
  }
})