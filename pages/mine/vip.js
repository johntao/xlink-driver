function is_vip(that){
  var user_id = wx.getStorageSync('user_id');
  wx.request({
    url: getApp().globalData.apiurl + 'get_vip_status',
    method: 'POST',
    data: {
      "user_id": user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.hideLoading();
      if (res.data.isSuccess == 1) {
        that.setData({
          is_vip:1
        })
        user_info(that)
      }
    }
  })
}

function user_info(that){
  var user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'user_info',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      console.log(res.data.data)
      that.setData({
        user: res.data.data
      });
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_vip:0
  },

  go_vip:function(){
    wx.navigateTo({
      url: '/pages/pay/vip'
    })
  },
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    is_vip(that);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})