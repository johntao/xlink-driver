var user_id = wx.getStorageSync('user_id');
function user_info(that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'user_info',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      that.setData({
        user: res.data.data
      });
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
    }
  })
}

function user_work(that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'user_work',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      that.setData({
        content: res.data.data
      });
      console.log(res.data.data);
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
    }
  })
}

function del(id,that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  getApp().sp();
  wx.request({
    url: getApp().globalData.apiurl + 'work_del',
    method: 'POST',
    data: {
      "user_id":user_id,
      "id":id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      user_work(that);
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
      wx.hideLoading();
    }
  })
}


Page({

  /**
   * 页面的初始数据
   */
  data: {
    user:{
      head_ico:'http://47.93.204.205/Public/xcximg/head.png'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    user_info(that);
    user_work(that);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    user_info(that);
    user_work(that);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return getApp().share();
  },
  worry:function(e){
    wx.navigateTo({
      url: '/pages/pay/check_type?id='+e.target.dataset.id+'&type=1'
    })
  },
  top:function(e){
    wx.navigateTo({
      url: '/pages/pay/check_type?id='+e.target.dataset.id+'&type=2'
    })
  },
  auth:function(e){
    wx.navigateTo({
      url: '/pages/mine/company'
    })
  },
  look:function(e){
     wx.navigateTo({
      url: '/pages/work/look?id='+e.target.dataset.id
    })
  },
  edit:function(e){
    wx.navigateTo({
      url: '/pages/work/edit?id='+e.target.dataset.id
    })
  },
  del:function(e){
    var that = this;
    wx.showModal({
      title: '提示',
      content: '删除后无法恢复，确认删除？',
      success: function(res) {
        if (res.confirm) {
          del(e.target.dataset.id,that);
        }
      }
    })
  },
})