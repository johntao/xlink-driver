var user_id = wx.getStorageSync('user_id');
var type;

function GetList(that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'my_subscribe',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      console.log(res);
      wx.hideNavigationBarLoading();
      type = res.data.type;
      for (var i = 0; i < type.length; i++) {
        type[i].show = "none";
      }
      that.setData({
        type:type
      })
    }
  })
}


Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    GetList(that);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return getApp().share();
  },
  show: function (e) {
    var that = this;
    console.log(type[e.target.dataset.id].show);
    if (type[e.target.dataset.id].show == "none") {
      type[e.target.dataset.id].show = "block";
    }else{
      type[e.target.dataset.id].show = "none";
    }
    that.setData({
      type:type
    })
    
    console.log(e.target.dataset.id);
  },
  checkboxChange: function (e) {
    var that = this;
    var checklist = e.detail.value;
    for (var i = 0; i < type.length; i++) {
      for (var j = 0; j < type[i].sub.length; j++) {
        type[i].sub[j].checked = 0;
        for (var k = 0; k < checklist.length; k++) {
          if (checklist[k] == type[i].sub[j].id) {
            type[i].sub[j].checked = 1;
            //return;
          }
        }
      }
    }
    that.setData({
      type:type
    })
  },
  formSubmit:function(e){
    getApp().set_form_id(1,e.detail.formId);
    wx.request({
      url: getApp().globalData.apiurl + 'save_subscribe',
      method: 'POST',
      data: {
        "user_id":user_id,
        "subscribe":"*"+e.detail.value.subscribe.join("*")+"*"
      },
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        if (res.data.isSuccess == 1) {
          wx.showToast({
            title: '修改成功',
            icon: 'success',
            duration: 800
          })
          wx.navigateBack({
            delta: 1
          })
        }else{
          getApp().alert(res.data.msg);
        }
      }
    })
  }
})