var change_thing = "";
var content = {};
var user_id = wx.getStorageSync('user_id');

function user_info(that,user_id){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'user_info',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {

      content = res.data.data;
      console.log(content);
      that.setData({
        content: content
      });
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
    }
  })
}

function set(name,value){
  user_id = wx.getStorageSync('user_id');
  wx.request({
    url: getApp().globalData.apiurl + 'set_user_info',
    method: 'POST',
    data: {
      "user_id":user_id,
      "name":name,
      "value":value
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      console.log(res);
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    content:{},
    bgshow:0,
    title:"您的新昵称",
    value:"中国IT的希望",
    focus:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    var that = this;
    user_info(that,user_id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return getApp().share();
  },
  look_head:function(e){
    console.log(e.target.dataset.img)
    wx.previewImage({
      current: e.target.dataset.img, // 当前显示图片的http链接
      urls: [e.target.dataset.img] // 需要预览的图片http链接列表
    })
  },
  changesex:function(){
    var that = this;
    wx.showActionSheet({
      itemList: ['男', '女'],
      success: function(res) {
        if (res.tapIndex == 1) {
          content.sex = 0;
          that.setData({
            content:content
          });
          set("sex",0);
        }else if (res.tapIndex == 0) {
          content.sex = 1;
          that.setData({
            content:content
          });
          set("sex",1);
        }
      }
    })
  },
  changehead:function(){
    var that = this;
    user_id = wx.getStorageSync('user_id');
    wx.chooseImage({
      count:1,
      sizeType:"compressed",
      success: function(res) {
        var tempFilePaths = res.tempFilePaths;
        content.head_ico = tempFilePaths;
        that.setData({
          content:content
        });
        wx.uploadFile({
          url: getApp().globalData.apiurl+'change_head',
          filePath: tempFilePaths[0],
          name: 'img',
          formData:{
            'user_id': user_id
          },
          success: function(res){
            console.log(res);
          }
        })
      }
    })
  },
  changenickname:function(e){
    change_thing = "nick_name";
    this.setData({
      title:"昵称",
      value:e.currentTarget.dataset.value,
      bgshow:1
    });
  },
  changerealname:function(e){
    change_thing = "real_name";
    this.setData({
      title:"正式姓名",
      value:e.currentTarget.dataset.value,
      bgshow:1
    });
  },
  changeage:function(e){
    change_thing = "age";
    this.setData({
      title:"年龄",
      value:e.currentTarget.dataset.value,
      bgshow:1
    });
  },
  changemobile:function(e){
    change_thing = "mobile";
    this.setData({
      title:"手机号码",
      value:e.currentTarget.dataset.value,
      bgshow:1
    });
  },
  changecontent:function(e){
    change_thing = "content";
    this.setData({
      title:"个性签名",
      value:e.currentTarget.dataset.value,
      bgshow:1
    });
  },
  changecommonrealname:function(e){
    if (e.detail.value == true) {
      var value = 1;
    }else {
      var value = 0;
    }
    set("common_real_name",value);
  },
  changecommonmobile:function(e){
    if (e.detail.value == true) {
      var value = 1;
    }else {
      var value = 0;
    }
    set("common_mobile",value);
  },
  changeallowpushmsg:function(e){
    if (e.detail.value == true) {
      var value = 1;
    }else {
      var value = 0;
    }
    set("allow_push_msg",value);
  },
  calce:function(){
    this.setData({
      bgshow:0
    });
  },
  formSubmit:function(e){
    console.log(e);
    getApp().set_form_id(1,e.detail.formId);
    if (change_thing == "nick_name") {
      content.nick_name = e.detail.value.title
      this.setData({
        bgshow:0,
        content:content
      });
      set(change_thing,e.detail.value.title);
    }else if (change_thing == "real_name") {
      content.real_name = e.detail.value.title
      this.setData({
        bgshow:0,
        content:content
      });
      set(change_thing,e.detail.value.title);
    }else if (change_thing == "age") {
      content.age = e.detail.value.title
      this.setData({
        bgshow:0,
        content:content
      });
      set(change_thing,e.detail.value.title);
    }else if (change_thing == "mobile") {
      content.mobile = e.detail.value.title
      this.setData({
        bgshow:0,
        content:content
      });
      set(change_thing,e.detail.value.title);
    }else if (change_thing == "content") {
      content.content = e.detail.value.title
      this.setData({
        bgshow:0,
        content:content
      });
      set(change_thing,e.detail.value.title);
    }
    this.setData({
      bgshow:0
    });
  }
})