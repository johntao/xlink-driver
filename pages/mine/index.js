var app = getApp();
var user_id = wx.getStorageSync('user_id');
function user_info(that,user_id){
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'user_info',
    method: 'POST',
    data: {
      "user_id":user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.stopPullDownRefresh();
      wx.hideNavigationBarLoading();
      if (res.data.isSuccess == 1) {
        that.setData({
          head_ico: res.data.data.head_ico
        });
        that.setData({
          name: res.data.data.name
        });
        that.setData({
          content: res.data.data.content
        });
      }else{
        getApp().alert(res.data.msg);
      }
      
      
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:"未登录",
    content:"点击登录",
    head_ico:"http://47.93.204.205/Public/xcximg/head.png"
  },
  location:function(){
    wx.navigateTo({
      url: '/pages/location/index'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (e) {

    wx.showShareMenu({
      withShareTicket: true
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      // app.alert('请先登录');
      // app.wxLogin();
    } else {
      var that = this;
      user_info(that,user_id);
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      app.alert('请先登录');
      app.wxLogin();
    } else {
      var that = this;
      user_info(that,user_id);
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return getApp().share();
  },
  sets:function(){
    var that = this;
    user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      wx.showModal({
        title: '提示',
        content: '请先登录！',
        cancelText:'暂不登录',
        confirmText:'一键登录',
        success: function(res) {
          if (res.confirm) {

            app.wxLogin(function(){
              user_id = wx.getStorageSync('user_id');
              user_info(that,user_id);
              setTimeout(function(){
                wx.navigateTo({
                  url: '/pages/mine/sets'
                })
              },500);
            });

          }
        }
      })
    } else {
      wx.navigateTo({
        url:'/pages/mine/sets'
      })
    }
  },
  subscribe:function(){
    var that = this;
    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      wx.showModal({
        title: '提示',
        content: '请先登录！',
        cancelText:'暂不登录',
        confirmText:'一键登录',
        success: function(res) {
          if (res.confirm) {

            app.wxLogin(function(){
              user_id = wx.getStorageSync('user_id');
              user_info(that,user_id);
              setTimeout(function(){
                wx.navigateTo({
                  url: '/pages/mine/subscribe'
                })
              },500);
            });
            
          }
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/mine/subscribe'
      })
    }
  },
  company:function(){
    var that = this;
    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      wx.showModal({
        title: '提示',
        content: '请先登录！',
        cancelText:'暂不登录',
        confirmText:'一键登录',
        success: function(res) {
          if (res.confirm) {

            app.wxLogin(function(){
              user_id = wx.getStorageSync('user_id');
              user_info(that,user_id);
              setTimeout(function(){
                wx.navigateTo({
                  url: '/pages/mine/company'
                })
              },500);
            });
            
          }
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/mine/company'
      })
    }
  },
  work:function(){
    var that = this;
    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      wx.showModal({
        title: '提示',
        content: '请先登录！',
        cancelText:'暂不登录',
        confirmText:'一键登录',
        success: function(res) {
          if (res.confirm) {

            app.wxLogin(function(){
              user_id = wx.getStorageSync('user_id');
              user_info(that,user_id);
              setTimeout(function(){
                wx.navigateTo({
                  url: '/pages/mine/work'
                })
              },500);
            });
            
          }
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/mine/work'
      })
    }
  },
  bbc:function(){
    var that = this;
    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      wx.showModal({
        title: '提示',
        content: '请先登录！',
        cancelText:'暂不登录',
        confirmText:'一键登录',
        success: function(res) {
          if (res.confirm) {

            app.wxLogin(function(){
              user_id = wx.getStorageSync('user_id');
              user_info(that,user_id);
              setTimeout(function(){
                wx.navigateTo({
                  url: '/pages/mine/bbc'
                })
              },500);
            });
            
          }
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/mine/bbc'
      })
    }
  },
  vip:function(){
    var that = this;
    var user_id = wx.getStorageSync('user_id');
    if (user_id == undefined || user_id == null || user_id == "") {
      wx.showModal({
        title: '提示',
        content: '请先登录！',
        cancelText:'暂不登录',
        confirmText:'一键登录',
        success: function(res) {
          if (res.confirm) {

            app.wxLogin(function(){
              user_id = wx.getStorageSync('user_id');
              user_info(that,user_id);
              setTimeout(function(){
                wx.navigateTo({
                  url: '/pages/mine/vip'
                })
              },500);
            });
            
          }
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/mine/vip'
      })
    }
  },
  auth:function(){
    wx.openSetting({
      success: (res) => {
        console.log(res);
      }
    })
  }
})