var util = require('../../utils/util.js');
var user_id;

function getAllAddressList (that) {
    getApp().sp();
    user_id = wx.getStorageSync('user_id');
    wx.request({
        url: getApp().globalData.apiurl + 'getAllAddressList',
        method: 'POST',
        data: {
            "user_id": user_id
        },
        header: {
            "content-type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
            console.log(res);
            var addresses = [];
            addresses.push("请选择");
            for (var i = 0; i < res.data.data.length; i++) {
                addresses.push(res.data.data[i]);
            }
            that.setData({
                addresses: addresses
            })
            getApp().hp();
            wx.stopPullDownRefresh();
        }
    })
}

function containerTest (container_no) {

    return true;
}

function uploadWorkOrder (container_no1, container_no2, address, bind_time, that) {
    getApp().sp();
    user_id = wx.getStorageSync('user_id');
    wx.request({
        url: getApp().globalData.apiurl + 'uploadWorkOrder',
        method: 'POST',
        data: {
            "user_id": user_id,
            "container_no1": container_no1,
            "container_no2": container_no2,
            "address": address,
            "bind_time": bind_time
        },
        header: {
            "content-type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
            console.log(res.data);
            if (res.data.isSuccess == 1) {
                uploadImg(that, res.data.id);
            }


        }
    })
}

function uploadImg (that, id) {
    if (that.data.files.length > 0) {
        wx.uploadFile({
            url: getApp().globalData.apiurl + 'uploadImg', //仅为示例，非真实的接口地址
            filePath: that.data.files[0],
            name: 'file',
            formData: {
                'id': id
            },
            success: function (res) {
                that.data.files.splice(0, 1);
                uploadImg(that, id);
            }
        })
    } else {
        getApp().hp();
        getApp().alert("工单提交成功！我们的客服人员会和您取得联系！");
        setTimeout(function () {
            wx.navigateBack({
                delta: 2
            })
        }, 1500);

        //console.log("完成!");
        return;
    }
}

Page({

    /**
     * 页面的初始数据
     */
    data: {
        addresses: ["请选择"],
        addressIndex: 0,
        files: [],
        inputVal1: "",
        inputVal2: ""
    },
    inputTyping1: function (e) {
        this.setData({
            inputVal1: e.detail.value.toUpperCase()
        });
    },
    inputTyping2: function (e) {
        this.setData({
            inputVal2: e.detail.value.toUpperCase()
        });
    },
    chooseImage: function (e) {
        var that = this;
        user_id = wx.getStorageSync('user_id');
        wx.chooseImage({
            count: 1,
            sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                that.setData({
                    files: that.data.files.concat(res.tempFilePaths)
                });
            }
        })
    },
    previewImage: function (e) {
        wx.previewImage({
            current: e.currentTarget.id, // 当前显示图片的http链接
            urls: this.data.files // 需要预览的图片http链接列表
        })
    },
    bindDateChange: function (e) {
        this.setData({
            date: e.detail.value
        })
    },
    addressChange: function (e) {
        this.setData({
            addressIndex: e.detail.value
        })
    },
    onLoad: function (options) {
        var date = util.formatTime(new Date());
        this.setData({
            date: date
        });
    },
    onShow: function () {
        var that = this;
        getAllAddressList(that);
    },
    formSubmit: function (e) {

        var post = e.detail.value;
        // if( ( post.container_no1!="" && !/^\d{7}$/.test(post.container_no1) ) || ( post.container_no2!="" && !/^\d{7}$/.test(post.container_no2) ) ){
        //   getApp().alert('箱号为7位数字');return;
        // }
        // if (!containerTest(post.container_no1)) {
        //   getApp().alert("箱号1输入错误，请检查！");return;
        // }
        // if (!containerTest(post.container_no2)) {
        //   getApp().alert("箱号2输入错误，请检查！");return;
        // }
        // if (post.container_no1 == "" && post.container_no2 == "") {
        //   getApp().alert("请至少填写一个箱号！");return;
        // }
        if (!(this.data.files.length > 0)) {
            getApp().alert("请至少上传一个图片！");
            return;
        }
        if (post.address == 0) {
            getApp().alert("请选择送货地址！");
            return;
        }
        // if (post.container_no1 == post.container_no2) {
        //     getApp().alert("请不要输入两个相同的箱号！");
        //     return;
        // }
        var that = this;
        uploadWorkOrder(post.container_no1, post.container_no2, this.data.addresses[post.address], post.bind_time, that);

    }
})