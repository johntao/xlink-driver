var user_id;


function getMineSubOrderList(that){
  user_id = wx.getStorageSync('user_id');
  getApp().sp("加载中...");
  wx.request({
    url: getApp().globalData.apiurl + 'getMineSubOrderList',
    method: 'POST',
    data: {
      "user_id": user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      that.setData({
        orders:res.data.data
      })
      getApp().hp();
    }
  })
}


Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  onShow: function () {
    var that = this;
    getMineSubOrderList(that);
  },

  onPullDownRefresh: function () {
    var that = this;
    getMineSubOrderList(that);
  },
  look:function(e){
    wx.navigateTo({
      url: '/pages/sub_order_info/sub_order_info?order_no='+e.currentTarget.dataset.order_no
    })
  },
  fan:function(e){
    wx.navigateTo({
      url: '/pages/bind_other/bind_other?heavy_or_empty=0&in_or_out=2&container_no='+e.currentTarget.dataset.container_no
    })
  },
  video:function(){
    wx.chooseVideo({
      sourceType: ['camera'],
      maxDuration: 10,
      camera: 'back',
      success: function(res) {
        that.setData({
          src: res.tempFilePath
        })
      }
    })
  }

  

})