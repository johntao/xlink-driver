var all = [];
var search = [];
var values = [];
var check = [];
var user_id;


function getAllContainerList(that){
  user_id = wx.getStorageSync('user_id');
  getApp().sp("加载中...");
  wx.request({
    url: getApp().globalData.apiurl + 'getAllContainerList',
    method: 'POST',
    data: {
      "user_id": user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {

      if (res.data.isSuccess == 1) {
        for (var i = 0; i < res.data.data.length ; i++) {
          res.data.data[i].checked = false;
        }
        all = res.data.data;
        search = res.data.data;
        that.setData({
          containers : search
        });
        getAllAddressList(that);
      }else{
        getApp().alert(res.data.res)
        getApp().hp();
      }
      
    }
  })
}

function getAllAddressList(that){
  user_id = wx.getStorageSync('user_id');
  wx.request({
    url: getApp().globalData.apiurl + 'getAllAddressList',
    method: 'POST',
    data: {
      "user_id": user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      if (res.data.isSuccess == 1) {
        var addresses = [];
        addresses.push("请选择");
        for (var i = 0; i < res.data.data.length; i++) {
          addresses.push(res.data.data[i]);
        }
        that.setData({
          addresses:addresses
        })
      }else{
        getApp().alert(res.data.res)
      }
      
      getApp().hp();
      wx.stopPullDownRefresh();
    }
  })
}

function bindHeavyContainer(address,check,that){
  getApp().sp("加载中...");
  var sub_order_no = [];
  for (var i = 0; i < check.length; i++) {
    sub_order_no.push(check[i].sub_order_no);
  }
  wx.request({
    url: getApp().globalData.apiurl + 'bindHeavyContainer',
    method: 'POST',
    data: {
      "user_id": user_id,
      "address":address,
      "check":sub_order_no
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      getApp().hp();
      if (res.data.isSuccess == 0) {
        getApp().alert(res.data.res);
      }else{
        getApp().alert(res.data.res);
        getAllContainerList(that);
      }
      
      console.log(res.data);
      

    }
  })
  //console.log(sub_order_no);
}


Page({

  /**
   * 页面的初始数据
   */
  data: {
      inputShowed: false,
      inputVal: "",
      addresses: ["请选择"],
      addressIndex: 0
  },
  showInput: function () {
      this.setData({
          inputShowed: true
      });
  },
  hideInput: function () {
      this.setData({
          inputVal: "",
          inputShowed: false
      });
  },
  clearInput: function () {
      this.setData({
          inputVal: ""
      });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });

    search = [];
    //console.log(all);
    for (var i = 0; i < all.length; i++) {

      if(all[i].container_no.indexOf(e.detail.value) >= 0 || all[i].checked == true){
        search.push(all[i]);
      }
    }
    
    this.setData({
      containers : search
    });
      
  },
  checkboxChange: function (e) {
    //console.log( e.detail.value);

    values = e.detail.value;
    check = [];
    for (var i = 0 ; i < search.length ; i++) {
        search[i].checked = false;

        for (var j = 0 ; j < values.length ; j++) {
            if(search[i].sub_order_no == values[j]){
                search[i].checked = true;
                break;
            }
        }
    }


    for (var i = 0 ; i < all.length ; i++) {
        all[i].checked = false;

        for (var j = 0 ; j < values.length ; j++) {
            if(all[i].sub_order_no == values[j]){
                check.push(all[i]);
                all[i].checked = true;
                break;
            }
        }
    }

    this.setData({
      containers : search
    });
  },
  addressChange: function(e) {
    this.setData({
        addressIndex: e.detail.value
    })
  },
  gongdan:function(){
    wx.navigateTo({
      url: '/pages/gongdan/gongdan'
    })
  },
  bind:function(e){

    //console.log(e.detail.value.address);
    if(e.detail.value.address == 0){
      getApp().alert("请选择送货地址");return;
    }

    var that = this;
    var address = this.data.addresses[e.detail.value.address];
    if (check.length > 2) {
      getApp().alert("请选择不多于2个箱子");return;
    }
    if (check.length == 0) {
      getApp().alert("请选择至少1个箱子");return;
    }
    var content = "箱号1:"+check[0].container_no;
    if (check.length == 2) {
      content = content+"\r 箱号2:"+check[1].container_no;
    }
    content = content+"\r 送货地址:"+address;

    wx.showModal({
      title: '请确认！录入错误会影响到您的结算！',
      content: content,
      cancelText:"我再想想",
      confirmText:"确定了",
      confirmColor:"#c71d23",
      success: function(res) {
        if (res.confirm) {
          that.setData({
            addressIndex:0
          })
          bindHeavyContainer(address,check,that);
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  onShow: function () {
    var that = this;
    getAllContainerList(that);
  },

  onPullDownRefresh: function () {

    this.setData({
      addressIndex:0
    })
    this.clearInput();
    var that = this;
    getAllContainerList(that);
  },

})