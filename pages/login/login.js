// pages/login/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {},
    onShow: function () {

    },
    formSubmit: function (e) {
        var post = e.detail.value;
        if (post.login_name == "") {
            getApp().alert("账号不能为空！");
            return;
        }
        if (post.pwd == "") {
            getApp().alert("密码不能为空！");
            return;
        }
        this.do_login(post.login_name, post.pwd);
    },
    do_login (login_name, pwd) {
        wx.login({
            success: function (res) {
                var code = res['code'];
                wx.getUserInfo({
                    success: function (info) {
                        var rawData = info['rawData'];
                        var signature = info['signature'];
                        var encryptedData = info['encryptedData'];
                        var iv = info['iv'];
                        getApp().sp("正在登录...");
                        wx.request({
                            url: getApp().globalData.apiurl + 'login',
                            method: 'POST',
                            dataType: "json",
                            header: {
                                "content-type": "application/x-www-form-urlencoded"
                            },
                            data: {
                                "code": code,
                                "rawData": rawData,
                                "signature": signature,
                                'iv': iv,
                                'encryptedData': encryptedData,
                                "login_name": login_name,
                                "pwd": pwd
                            },
                            success: function (res) {
                                var data = res.data.data;
                                if (+res.data.isSuccess != 1) {
                                    getApp().hp();
                                    return
                                }
                                wx.setStorageSync('user_id', data.id);
                                wx.setStorageSync('fleet_name', data.fleet_name);
                                wx.setStorageSync('head_ico', data.head_ico);
                                wx.setStorageSync('login_name', data.login_name);
                                wx.setStorageSync('mobile', data.mobile);
                                wx.setStorageSync('real_name', data.real_name);
                                wx.setStorageSync('truck_no', data.truck_no);
                                wx.setStorageSync('pwd', pwd);
                                wx.setStorageSync('need_pad', data.need_pad);
                                wx.setStorageSync('end_station_type', data.end_station_type);
                                wx.setStorageSync('n_station', data.n_station);

                                getApp().getToken();
                                getApp().hp();
                                // wx.redirectTo({
                                //     url: '/pages/index/index'
                                // })
                                wx.navigateBack({
                                  delta: 1
                                })
                            },
                            complete: function (res) {
                                getApp().hp()
                                if (+res.data.isSuccess !== 1) {
                                    wx.showModal({
                                        title: '提示',
                                        content: res.data.res,
                                        showCancel: false,
                                        success: function (res) {
                                        }
                                    })
                                }
                            }
                        });
                    }
                });

            }
        });
    }
})