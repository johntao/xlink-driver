var voucherImg = []
var id1 = ''
var id2 = ''
var id3 = ''
var container_list = [] // 箱号列表
var order = {}
var isAddMedia = false
var containerCheck = require('../../utils/containerCheck.js')

var takeoutService = require('../../service/takeoutService.js')


Page({

  /**
   * 页面的初始数据
   */
  data: {
    numRange:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
    num1:1,
    num2:1,
    num3:1,
    isAddMedia: false,
    isEdit1: false,
    isEdit2: false,
    isEdit3: false,
    list1: [],
    list2: [],
    list3: [],
    inputVal1: "",
    inputVal2: "",
    inputVal3: "",
    voucher_img: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    isAddMedia = !!options.isAddMedia // 是否编辑模式
    order = {}
    voucherImg = []
    container_list = []
    //获取ID
    this.getDriverOderdByState();
    
  },

  getDriverOderdByState: function(){
    let that =this;
    takeoutService.getDriverOderdByState({ driverId: 1 }).then((res) => {
      console.log(res);
      if(res.isSucces <1)
      {
        getApp().alert(res.data.res)
        return;
      }
      container_list = []
      res.data.data.rows.forEach((item) => {
        container_list.push({ 'container_no': item.containerNo, 'id': item.subOrderId })
      })
      that.setData({
        list1: container_list,
        list2: container_list,
        list3: container_list
      })
    })
  },

  handlePicker1Change: function(e){
    let that = this;
    this.setData({
      num1: that.data.numRange[e.detail.value]
    })
  },
  handlePicker2Change: function (e) {
    let that = this;
    this.setData({
      num2: that.data.numRange[e.detail.value]
    })
  },
  handlePicker3Change: function (e) {
    let that = this;
    this.setData({
      num3: that.data.numRange[e.detail.value]
    })
  },

  inputTyping1: function (e) {
    id1 = ''
    let list_now = []
    let inputVal = e.detail.value.toUpperCase()
    container_list.forEach(function (data) {
      if (data.container_no && data.container_no.indexOf(inputVal) >= 0) {
        list_now.push(data)
      }
    })
    this.setData({
      inputVal1: inputVal,
      list1: list_now,
      isEdit1: true
    })
    if (e.detail.value === "") {
      
      this.setData({
        list1: container_list,
        isEdit1: false
      })
    }
  },
  setVal1: function (e) {
    console.log(e);
    id1 = e.currentTarget.dataset.id
    console.log(id1);
    this.setData({
      inputVal1: e.currentTarget.dataset.container_no,
      isEdit1: false
    })
  },
  inputTyping2: function (e) {
    id2 = ''
    let list_now = []
    let inputVal = e.detail.value.toUpperCase()
    container_list.forEach(function (data) {
      if (data.container_no && data.container_no.indexOf(inputVal) >= 0) {
        list_now.push(data)
      }
    })
    this.setData({
      inputVal2: inputVal,
      list2: list_now,
      isEdit2: true
    })
    if (e.detail.value === "") {
      
      this.setData({
        list2: container_list,
        isEdit2: false
      })
    }
  },
  setVal2: function (e) {
    id2 = e.currentTarget.dataset.id
    this.setData({
      inputVal2: e.currentTarget.dataset.container_no,
      isEdit2: false
    })
  },
  inputTyping3: function (e) {
    id3 = ''
    let list_now = []
    let inputVal = e.detail.value.toUpperCase()
    container_list.forEach(function (data) {
      if (data.container_no && data.container_no.indexOf(inputVal) >= 0) {
        list_now.push(data)
      }
    })
    this.setData({
      inputVal3: inputVal,
      list3: list_now,
      isEdit3: true
    })
    if (e.detail.value === "") {
      
      this.setData({
        list3: container_list,
        isEdit3: false
      })
    }
  },
  setVal3: function (e) {
    id3 = e.currentTarget.dataset.id
    this.setData({
      inputVal3: e.currentTarget.dataset.container_no,
      isEdit3: false
    })
  },
  bind: function (e) {
    let post = e.detail.value
      if (!containerCheck.methods.checkDigit(post.container_no1)) {
        getApp().alert("箱号1输入错误！")
        return
      } else {
        let is_in = false
        container_list.forEach(function (data) {
          if (data.container_no === post.container_no1) {
            is_in = true
          }
        })
        if (!is_in) {
          getApp().alert("箱号1输入错误！请选择列表中的箱号！")
          return
        }
      }
      if (!post.container_no2) {
        post.container_no2 = ""
      } else {
        if (!containerCheck.methods.checkDigit(post.container_no2)) {
          getApp().alert("箱号2输入错误！")
          return
        } else {
          let is_in = false
          container_list.forEach(function (data) {
            if (data.container_no === post.container_no2) {
              is_in = true
            }
          })
          if (!is_in) {
            getApp().alert("箱号2输入错误！请选择列表中的箱号！")
            return
          }
        }
      }
    if ((post.container_no1 === post.container_no2) || (post.container_no1 === post.container_no3 && post.container_no3 !== "") || (post.container_no2 === post.container_no3 && post.container_no2 !== "" && post.container_no3 !=="") ) {
        getApp().alert("请不要输入两个相同的箱号！")
        return
      }
     post.container_num1 = this.data.num1;
    post.container_num2 = this.data.num2;
    post.container_num3 = this.data.num3;
    let data =[];
    if (post.container_no1!="")
    {
      let container1 = { containerNum: this.data.num1, containerNo: post.container_no1, subOrderId:id1 };
      data.push(container1);
    }
    if (post.container_no2 != "") {
      let container2 = { containerNum: this.data.num2, containerNo: post.container_no2 ,subOrderId: id2 };
    data.push(container2);
    }

    if (post.container_no3 != "") {
      let container3 = { containerNum: this.data.num3, containerNo: post.container_no3, subOrderId: id3};
    data.push(container3);
    }


    this.saveSubOrder(data)
  },
  saveSubOrder: function (data) {
    console.log(data);
    getApp().sp("加载中...")
    takeoutService.saveTakeOutContainer(data).then((res)=>{
      console.log(res);
      getApp().hp();
      if (res.data.isSuccess === 1){
        wx.showModal({
          title: '提示',
          content: '上传成功',
          success: function (res) {
            wx.navigateBack({
              delta: 1
            })
          }
        })
      }else{
        wx.showModal({
          title: '错误',
          content: res.data.res,
          success: function (res) {
          }
        })
      }
     
    })

    return;
  }
})