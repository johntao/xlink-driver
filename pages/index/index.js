var user_id;

function getUserInfo (that) {
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'getUserInfo',
    method: 'POST',
    data: {
      "user_id": user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.setStorageSync('fleet_name', res.data.data.fleet_name);
      wx.setStorageSync('head_ico', res.data.data.head_ico);
      wx.setStorageSync('login_name', res.data.data.login_name);
      wx.setStorageSync('mobile', res.data.data.mobile);
      wx.setStorageSync('real_name', res.data.data.real_name);
      wx.setStorageSync('truck_no', res.data.data.truck_no);
      wx.hideNavigationBarLoading();

      that.setData({
        data: {
          head_ico: res.data.data.head_ico,
          login_name: res.data.data.login_name,
          fleet_name: res.data.data.fleet_name,
          truck_no: res.data.data.truck_no,
          end_station_type: wx.getStorageSync('end_station_type')
        }
      })
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    noread: 0,
    data: {
      head_ico: "/img/head.png",
      login_name: "姓名",
      fleet_name: "车队",
      truck_no: "车牌号"
    },
    grids: [0, 1, 2, 3, 4, 5, 6, 7, 8]
  },
  onShow: function () {
    var that = this;
    getUserInfo(that);

    getApp().checkLogin();
    user_id = wx.getStorageSync('user_id');
    this.setData({
      data: {
        head_ico: wx.getStorageSync('head_ico'),
        login_name: wx.getStorageSync('login_name'),
        fleet_name: wx.getStorageSync('fleet_name'),
        truck_no: wx.getStorageSync('truck_no'),
        end_station_type: wx.getStorageSync('end_station_type')
      }
    })
    getApp().addListenerindex(function (data) {
      // console.log(data);
      // console.log(data.friend.length);
      // console.log(data.group.length);
      that.setData({
        noread: data.friend.length + data.group.length
      })
    });

  },
  bind_container: function (e) {
    var ret = e.currentTarget.dataset;
    wx.navigateTo({
      url: '/pages/bind_other/bind_other?heavy_or_empty=' + ret.he + '&in_or_out=' + ret.io
    })
  },
  heavy_out: function (e) {
    wx.navigateTo({
      url: '/pages/heavy_out/heavy_out'
    })
  },
  handleTackOutHistory: function (e) {
    wx.navigateTo({
      url: '/pages/take_out_history/take_out_history'
    })
  },
  handleTackOut: function (e) {
    wx.navigateTo({
      url: '/pages/take_out/take_out'
    })
  },
  zhong: function () {
    wx.navigateTo({
      url: '/pages/zhong/zhong'
    })
  },
  kong: function () {
    wx.navigateTo({
      url: '/pages/kong/kong'
    })
  },
  tapPickupOrder:function(){
    wx.navigateTo({
      url: '/pages/pickupOrder/pickupOrder'
    })
  },
  order: function () {
    if (this.data.data.end_station_type == 'shz') {
      wx.navigateTo({
        url: '/pages/historyOrder/historyOrder'
      })
    } else if (this.data.data.end_station_type == 'krl') {
      wx.navigateTo({
        url: '/pages/order/order'
      })
    }
  },
  history_order_list: function () {
    wx.navigateTo({
      url: '/pages/history_order_list/history_order_list'
    })
  },
  change_car: function () {
    wx.navigateTo({
      url: '/pages/change_car/change_car'
    })
  },
  user_info: function () {
    wx.navigateTo({
      url: '/pages/user_info/user_info'
    })
  },
  chat_list: function () {
    wx.navigateTo({
      url: '/pages/chat_list/chat_list'
    })
  },
  set_truck: function () {
    wx.navigateTo({
      url: '/pages/set_truck/set_truck'
    })
  },
  login_out: function () {
    wx.clearStorageSync();
    wx.navigateTo({
      url: '/pages/login/login'
    })
  },
  call: function () {
    wx.showActionSheet({
      itemList: ['客服 杨洋-18599078572', '客服 时祥-18130899820'],
      success: function (res) {
        if (res.tapIndex == 0) {
          wx.makePhoneCall({
            phoneNumber: '18599078572'
          })
        } else if (res.tapIndex == 1) {
          wx.makePhoneCall({
            phoneNumber: '18130899820'
          })
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  }
})