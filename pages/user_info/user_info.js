var user_id;
function getUserInfo(that){
  user_id = wx.getStorageSync('user_id');
  wx.showNavigationBarLoading();
  wx.request({
    url: getApp().globalData.apiurl + 'getUserInfo',
    method: 'POST',
    data: {
      "user_id": user_id
    },
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    success: function (res) {
      wx.hideNavigationBarLoading();
      let this_data = {
        login_name: res.data.data.login_name ? res.data.data.login_name : '暂未填写',
        real_name: res.data.data.real_name ? res.data.data.real_name : '暂未填写',
        mobile: res.data.data.mobile ? res.data.data.mobile : '暂未填写',
        email: res.data.data.email ? res.data.data.email : '暂未填写',
        fleet_name: res.data.data.fleet_name ? res.data.data.fleet_name : '暂未填写',
        truck_no: res.data.data.truck_no ? res.data.data.truck_no : '暂未填写',
      }
      that.setData({
        data: this_data
      })
    }
  })
}

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    getUserInfo(that)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})