
//获取接单列表
function getOrderList(params) {
  if (!params.hasOwnProperty("pageSize"))
  {
    params["pageSize"]=20;
  }
  if (!params.hasOwnProperty("pageNumber")) {
    params["pageNumber"]=1
  }
  console.log(params);
  return new Promise(function (resolve, reject) {
    getApp().requestPromise(getApp().globalData.javaurl + "/main/station/getSubOrderList",params).then(function(res){
      resolve(res);
    },function(err){
      reject(err);
    })
  })
}


//设置接单子订单接受
function setAcceptSubOrder(params) {
  return new Promise(function (resolve, reject) {
    getApp().requestPromise(getApp().globalData.javaurl + "/main/station/setAcceptSubOrder", params).then(function (res) {
      resolve(res);
    }, function (err) {
      reject(err);
    })
  })
}

//设置接单子订单接受
function setCancleSubOrder(params) {
  return new Promise(function (resolve, reject) {
    getApp().requestPromise(getApp().globalData.javaurl + "/main/station/setCancleSubOrder", params).then(function (res) {
      resolve(res);
    }, function (err) {
      reject(err);
    })
  })
}


module.exports ={
  getOrderList: getOrderList,
  setAcceptSubOrder: setAcceptSubOrder,
  setCancleSubOrder: setCancleSubOrder
}