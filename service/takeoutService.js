//获取箱号列表
export function getDriverOderdByState (params) {
  return new Promise(function (resolve, reject) {
    getApp().requestPromise(getApp().globalData.javaurl + "/main/driverOrderDetail/getTakeOutByDriverOrderLIstByPageV2", params,{},'POST').then(function (res) {
      resolve(res);
    }, function (err) {
      reject(err);
    })
  })
}

//保存掏箱
export function saveTakeOutContainer(params) {
  return new Promise(function (resolve, reject) {
    getApp().requestPromise(getApp().globalData.javaurl + "/main/dirverTakeOutContainer/saveTakeOutContainer", params, {}, 'POST').then(function (res) {
      resolve(res);
    }, function (err) {
      reject(err);
    })
  })
}

//掏箱历史
export function getTakeOutContainerByDriverByPage(params) {
  if (!params.hasOwnProperty("pageSize")) {
    params["pageSize"] = 20;
  }
  if (!params.hasOwnProperty("pageNumber")) {
    params["pageNumber"] = 1
  }
  return new Promise(function (resolve, reject) {
    getApp().requestPromise(getApp().globalData.javaurl + "/main/dirverTakeOutContainer/getTakeOutContainerByDriverByPage", params, { "content-type":"application/x-www-form-urlencoded"}, 'POST').then(function (res) {
      resolve(res);
    }, function (err) {
      reject(err);
    })
  })
}

//撤销
export function destoryTakeOutContainer(params) {
  return new Promise(function (resolve, reject) {
    getApp().requestPromise(getApp().globalData.javaurl + "/main/dirverTakeOutContainer/destoryTakeOutContainer", params, { "content-type": "application/x-www-form-urlencoded" }, 'POST').then(function (res) {
      resolve(res);
    }, function (err) {
      reject(err);
    })
  })
}