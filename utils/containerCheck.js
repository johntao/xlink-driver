/**
 * Created By zhangQ 2018/5/28
 */

let methods = {
  checkDigit(containerNo) {
    if (!(containerNo && containerNo.length === 11)) {
      return false
    }
    let codeObj = {
      A: '10',
      B: '12',
      C: '13',
      D: '14',
      E: '15',
      F: '16',
      G: '17',
      H: '18',
      I: '19',
      J: '20',
      K: '21',
      L: '23',
      M: '24',
      N: '25',
      O: '26',
      P: '27',
      Q: '28',
      R: '29',
      S: '30',
      T: '31',
      U: '32',
      V: '34',
      W: '35',
      X: '36',
      Y: '37',
      Z: '38'
    }
    let positon = 1
    let sum = 0
    for (let i = 0, leni = containerNo.length - 1; i < leni; i++) {
      let charCode = containerNo[i].toUpperCase()
      let re = new RegExp('^[a-zA-Z]+$')
      if (re.test(charCode)) {
        sum += +codeObj[charCode] * Math.pow(2, positon - 1)
      } else {
        sum += +charCode * Math.pow(2, positon - 1)
      }
      positon++
    }
    let checkdigit = sum % 11 % 10
    return +checkdigit === +containerNo[containerNo.length - 1]
  }
}

module.exports = {methods}